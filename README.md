# Client folder
First, run `npm ci` to install the dependencies.
Run `npm run ng serve` to start the dev server.
Run `npm run ng build` to build the sources.

# Server folder
Run an nginx server using the nginx.conf file to serve the front-end.
Run `cargo run` to compile and host the Rust server.

# Technical details
The websocket to communicate in-between the client side and the back-end side will run on port `4321` by default.  

The protocol upgrade for the websocket is made by nginx. To change this port, edit it on the Rust server side, in main.rs, and inside the nginx conf file too.

Nginx will serve the dist folder of the client side by default. You don't need to have a ng server running in prod.

You can test the websocket with something like `wscat` (deb package) by running cargo by itself.