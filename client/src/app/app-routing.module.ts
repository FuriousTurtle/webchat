import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TextWindowComponent } from './text-window/text-window.component'
import { LoginWindowComponent } from './login-window/login-window.component';

const routes: Routes = [
  {path:'login', component: LoginWindowComponent},
  {path:'', redirectTo: '/login', pathMatch: 'full'},
  {path:'chat/:user', component: TextWindowComponent},
  {path:'chat/*', redirectTo: '/login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
