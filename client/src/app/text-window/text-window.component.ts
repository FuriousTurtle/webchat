import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { ActivatedRoute } from '@angular/router';
import io from 'socket.io-client';

@Component({
  selector: 'app-text-window',
  templateUrl: './text-window.component.html',
  styleUrls: ['./text-window.component.css']
})

@Injectable()
export class TextWindowComponent implements OnInit {

  @ViewChild('messageBox') div: ElementRef;

  public messages;
  public currMessage;
  public currTimestamp;
  public sentMessage = {};
  public serverMessages = [];
  public userList = [];
  public socket = io.connect('');
  
  constructor(public http: HttpClient, private route: ActivatedRoute) { 
    this.http = http;
  }

  ngOnInit() {

    this.currMessage = "";

    this.socket
    .on('init', (message) => {
      this.serverMessages = message;
      var logs = {'user_name': localStorage.getItem('localUserName')}
      this.socket.emit('userInfo', logs);
    })
    .on('message', (message) => {
      this.serverMessages = message;
    })
    .on('updateUsers', (message) => {
      this.userList = message;
    })
    .on('disconnect', () => {
      this.socket.emit('disconnectUser', localStorage.getItem('localUserName'));
      this.userList = [];
      this.socket.removeAllListeners();
    })


  }

  getTime() {
    var timeString;
    var timeNow = new Date();
    var hours   = timeNow.getHours();
    var minutes = timeNow.getMinutes();
    timeString = "" + hours;
    timeString  += ((minutes < 10) ? ":0" : ":") + minutes;
    return timeString;
  }

  sendMessage(){
    if (this.currMessage != "" || undefined) {
    this.currTimestamp = this.getTime();
    this.socket.emit('newMessage', {user_name: localStorage.getItem('localUserName'), user_message: this.currMessage, user_message_time: this.currTimestamp});
    this.currMessage = "";
    }
    console.log(this.serverMessages);
  }

  checkKey($event) {
    var keycode = $event.keyCode || $event.which;
    if (keycode == '13') {
      this.sendMessage();
      return false;
    }
  }
  

}