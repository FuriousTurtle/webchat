import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-window',
  templateUrl: './login-window.component.html',
  styleUrls: ['./login-window.component.css']
})
export class LoginWindowComponent implements OnInit {

  constructor(private router: Router) { }

  public userName: string;

  ngOnInit() {
    if('localUserName' in localStorage) {
      this.userName = localStorage.getItem('localUserName');
    }
  }

  getNickname(){
    localStorage.setItem('localUserName', this.userName);
    this.router.navigate(['chat', this.userName]);
  }

}
