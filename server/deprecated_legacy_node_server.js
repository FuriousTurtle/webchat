const express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');
const server = express();
const isDev = false;
var io;

var router = express.Router();

function connectSecured(){
    var payload = {key: fs.readFileSync("/etc/letsencrypt/live/boubou.app/privkey.pem"), cert: fs.readFileSync("/etc/letsencrypt/live/boubou.app/fullchain.pem")};
    
    const httpsServer = https.createServer(payload, server);
    httpsServer.listen(443, function() {
        console.log((new Date()) + ' Server is listening on port 443');
    });
    io = require ('socket.io')(httpsServer);
}

function connectUnsecured(){
    const httpServer  = http.createServer(server);
    httpServer.listen(8070, function() {
        console.log((new Date()) + ' Server is listening on port 8070');
    });
    io = require ('socket.io')(httpServer);
}

isDev ? connectUnsecured() : connectSecured();

var messageCache = [{user_name: 'System', user_color: 'black', user_message: 'Initialisation du tchat', user_message_time: 'Now'}];
var userList = [];
var colorList = ['#5499C7', '#A569BD', '#CD6155', '#48C9B0', '#58D68D', '#F4D03F', '#E59866'];

router.get('/', function(req, res){
    server.use(express.static(__dirname + '/webChat/dist/webChat'));
    res.sendFile(__dirname  + '/webChat/dist/webChat/index.html');
});

router.get('/chat/*', function(req, res, next){
    isDev ? res.status(404).redirect('/') : res.status(404).redirect('https://boubou.app');
});

router.get('/login', function(req, res, next){
    isDev ? res.status(404).redirect('/') : res.status(404).redirect('https://boubou.app');
});

server.use(router);

io.on('connection', (socket) => {

    socket.emit('init', messageCache);

    socket
    .on('newMessage', message => {
        if (messageCache.length >= 20) {
            messageCache.pop();
        }
        messageCache.unshift(message);
        updateMessageColors()
        io.emit('message', messageCache);
    })
    .on('userInfo', message => {
        var randomColor = colorList[Math.floor((Math.random()) * (colorList.length))];
        var userAndColor = { user_name: message.user_name, user_color: randomColor };
        userList.unshift(userAndColor);
        io.emit('updateUsers', userList);
        socket.username = message.user_name;
    })
    .on('disconnect', message => {
        for (var i = 0; i < userList.length; i++){
            if(userList[i].user_name == socket.username)
            userList.splice(i, 1);
        }
        io.emit('updateUsers', userList);
    });

  });

  function updateMessageColors(){
      for (var i = 0; i < userList.length; i++){
          for (var j = 0; j < messageCache.length; j++){
            if (userList[i].user_name == messageCache[j].user_name){
                messageCache[j].user_color = userList[i].user_color;
            }
          }
      }
  }