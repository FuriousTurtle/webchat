use crate::m_user;

pub struct Message<'user> {
    user: &'user m_user::User,
    user_message: String,
    user_message_time: String,
}

pub struct MessageModule<'user> {
    m_mem: Vec<Message<'user>>,
}

impl<'user> MessageModule<'user> {
    pub fn create_message(
        &mut self,
        user: &'user m_user::User,
        user_message: String,
        user_message_time: String,
    ) -> () {
        let message = Message {
            user,
            user_message,
            user_message_time,
        };
        self.m_mem.push(message)
    }

    pub fn get_all_messages_from_memory(self) -> std::vec::IntoIter<Message<'user>> {
        return self.m_mem.into_iter();
    }
}
