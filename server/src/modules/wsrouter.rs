use std::thread::spawn;
use std::net::TcpStream;
use tungstenite::{WebSocket, Message::Text, server::accept};

pub struct WsRouter ();

impl WsRouter {
    
    pub fn open_ws(self, stream: TcpStream) -> () {
        spawn(move || {
            let mut websocket = accept(stream).unwrap();
            self.send_message_into_websocket(&mut websocket, "Websocket succesfully opened, waiting for client messages");
            loop {
                self.wsmessage_handler(&mut websocket)
            }
        });
    }

    fn wsmessage_handler(&self, websocket: &mut WebSocket<TcpStream>) -> () {
        let msg = websocket.read_message().unwrap();
                
        if msg.is_text() {
            match msg.into_text() {
                Ok(msg) => self.route_message(msg, websocket),
                Err(e) => panic!("{0}", e)
            }
        }
    }

    fn route_message(&self, message: String, websocket: &mut WebSocket<TcpStream>) -> () {
        match message.as_str() {
            "ping" => self.send_message_into_websocket(websocket, "pong"),
            _ => println!("Unrouted message received")
        }
    }

    fn send_message_into_websocket(&self, websocket: &mut WebSocket<TcpStream>, message: &str) -> () {
        match websocket.write_message(Text(String::from(message))) {
            Ok(_v) => println!("Message sent to websocket"),
            Err(e) => println!("{0}", e)
        }
    }
}