pub struct User {
    user_name: String,
    user_color: String,
}

pub struct UserModule {
    u_mem: Vec<User>,
}
impl UserModule {
    pub fn get_user<'user>(&'user self, user_name: String) -> Option<&'user User> {
        return self.u_mem.iter().find(|user| user.user_name == user_name);
    }

    pub fn create_user(&mut self, user_name: String, user_color: String) -> () {
        let user = User {
            user_name,
            user_color,
        };
        self.u_mem.push(user);
    }

    pub fn remove_user(&mut self, user_name: String) -> () {
        self.u_mem.retain(|user| user.user_name == user_name);
    }

    pub fn get_all_users(self) -> std::vec::IntoIter<User> {
        return self.u_mem.into_iter();
    }
}
