#![feature(proc_macro_hygiene, decl_macro)]
#[path = "modules/message.rs"] pub mod m_message;
#[path = "modules/user.rs"] pub mod m_user;
#[path = "modules/wsrouter.rs"] pub mod m_wsrouter;

use m_wsrouter::WsRouter;
use std::net::TcpListener;

fn main() {

    let server = TcpListener::bind("127.0.0.1:4321").unwrap();
    for stream in server.incoming() {
        match stream {
            Ok(stream) => {
                initialize_user_router().open_ws(stream);
            }
            Err(e) => {
                println!("Cannot open websocket -> {0}", e)
            }
        }
    }

    fn initialize_user_router() -> WsRouter {
        let router = WsRouter();
        return router;
    }
}
